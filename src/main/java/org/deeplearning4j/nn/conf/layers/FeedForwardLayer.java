package org.deeplearning4j.nn.conf.layers;

/**
 * Created by jeffreytang on 7/21/15.
 */

public abstract class FeedForwardLayer extends Layer {
    protected int nIn;
    protected int nOut;
   
    
    public int getNIn() {
		return nIn;
	}

	public int getNOut() {
		return nOut;
	}

	public void setNIn(int nIn) {
		this.nIn = nIn;
	}

	public void setNOut(int nOut) {
		this.nOut = nOut;
	}

	public FeedForwardLayer() {
    	
    }
    
    public FeedForwardLayer( Builder builder ){
    	super(builder);
    	this.nIn = builder.nIn;
    	this.nOut = builder.nOut;
    }

    public abstract static class Builder<T extends Builder<T>> extends Layer.Builder<T> {
        protected int nIn = 0;
        protected int nOut = 0;

        public T nIn(int nIn) {
            this.nIn = nIn;
            return (T) this;
        }

        public T nOut(int nOut) {
            this.nOut = nOut;
            return (T) this;
        }
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + nIn;
		result = prime * result + nOut;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FeedForwardLayer other = (FeedForwardLayer) obj;
		if (nIn != other.nIn)
			return false;
		if (nOut != other.nOut)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FeedForwardLayer [nIn=" + nIn + ", nOut=" + nOut
				+ ", activationFunction=" + activationFunction + ", weightInit="
				+ weightInit + ", dist=" + dist + ", dropOut=" + dropOut
				+ ", updater=" + updater + "]";
	}
    
    
}
